# Assignment for [Dott](https://ridedott.com/) Frontend Engineer Role

## Task 1

### Setup
Open your terminal and run `npm install`

### Results
To view the code results, run `npm start` 

### Tests
To run tests, run `npm test`

---
## Task 2

### Mockup
[Click this link to view the mockup of the page in Figma](https://www.figma.com/file/JrstTe8bUPX5mACo3VJeH1/Dott?node-id=0%3A1)

### Live Demo
[Click this link to view the demo of the app](https://focused-pike-758bb9.netlify.app/)

### Code Setup
`cd task-2 && npm install`

### Start the app locally
`npm start`

### Build the app
`npm run build`

### Tests
`npm test`

### Requirements

- [x] Use Typescript
- [x] Focus on mordern browser APIs
- [x] Search bar should support both voice and text input
- [x] Fetch book covers from the Open Library API
- [x] Autoscroll the carousel infinitely
- [x] Stop scrolling when the webpage is in the background.
- [x] Load images lazily as the carousel scrolls with a small buffer of a few images.
- [ ] Relative time of a moment when the last search query was performed should be updated
automatically.
- [ ] Display the relative time in a user's preferred language (navigator.language).
- [ ] Write Unit tests.

### Extra Requirements
- [ ] Give user visual feedback when they're speaking.
- [ ] Users should be able to click a book to view the book details page.
- [ ] User should be able to see more book details like the title and author.