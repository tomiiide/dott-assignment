"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var add_1 = require("../src/add");
var testNumber = 8;
var addNumber = add_1.default(testNumber);
test(testNumber + " + 100 should be " + (testNumber + 100), function () {
    expect(addNumber(100)).toEqual(testNumber + 100);
});
test(testNumber + " + 7 should be " + (testNumber + 7), function () {
    expect(addNumber(7)).toEqual(testNumber + 7);
});
//# sourceMappingURL=add.spec.js.map