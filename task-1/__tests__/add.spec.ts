import  addN  from '../src/add'

const testNumber = 8;
const addNumber = addN(testNumber)

test(`${testNumber} + 100 should be ${testNumber + 100}`, () => {
    expect(addNumber(100)).toEqual(testNumber + 100)
})

test(`${testNumber} + 7 should be ${testNumber + 7}`, () => {
    expect(addNumber(7)).toEqual(testNumber + 7)
})
