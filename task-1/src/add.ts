export default function addN(base : number){
    return function(arg : number){
       return base + arg
    }
}
const addEight = addN(8);
console.log("100 + 8 = "+addEight(100));
console.log("7 + 8 = "+addEight(7));
