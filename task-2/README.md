# DOTT Search

Installation
------------
```
git clone https://gitlab.com/tomiiide/dott-assignment.git
npm install
```

Getting Started
------------
* Start Dev (watch the `src` folder on file change)
  ```
  npm start
  ```
* Browse 
  ```
  http://localhost:3000
  ```
* Build for production
  ```
  npm run build
  ```