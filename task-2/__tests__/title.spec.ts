import Title from '../src/title'
import { createEvent, fireEvent, screen } from "testing-library__dom";
import { fixture, expect, html, elementUpdated, } from "@open-wc/testing";


window.customElements.define('search-title', Title);


describe("Title", () => {
    beforeEach(async () => {

    });

    it("should render an empty string when stuff is loading ", async () => {
        const el = await fixture(
            html`
                <search-title></search-title>
            `
        );
        await elementUpdated(el);
        await expect(el.innerHTML).to.be.empty;
    })

    it("should render title when event is fired", async () => {
        const el = await fixture('<search-title></search-title>')
        
        await elementUpdated(el);

        await fireEvent(document, new CustomEvent('onSearchBegin', { detail: { value: 'stuff' } }))
        console.log(el.innerHTML);
    })
})