module.exports = {
    preset: "ts-jest/presets/js-with-babel",
    testRegex: '(/__tests__/.*|\\.(test|spec))\\.(ts|tsx)$',
    transformIgnorePatterns: [
        "node_modules/(?!(testing-library__dom|chai-a11y-axe|@open-wc|lit-html|lit-element|pure-lit|lit-element-state-decoupler)/)"
    ],
    "setupFilesAfterEnv": [
        "./src/testSetup.ts"
    ],
    globals: {
        'ts-jest': {
            tsconfig: 'tsconfig.test.json',
        },
    },
}