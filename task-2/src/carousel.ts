
const styles = `

@media (min-width: 768px) { 
    .carousel-wrapper {
        width: 90%;
    }
}

.carousel-wrapper {
    overflow: hidden;
    width: 100%;
    height: 80vh;
  }

  .carousel-wrapper * {
    box-sizing: border-box;
  }

  .carousel {
    transform-style: preserve-3d;
  }

  .slides {
    transform-style: preserve-3d;   
    display: flex;
  }

  .carousel__photo {
    opacity: 0;
    position: absolute;
    top:0;
    margin: auto;
    padding: 1rem 4rem;
    z-index: 100;
    transition: transform .5s, opacity .5s, z-index .5s;
    height: 100%;
    min-height: 100px;
  }

  .carousel__photo.initial,
.carousel__photo.active {
  opacity: 1;
  position: relative;
  z-index: 900;
}

.carousel__photo.prev,
.carousel__photo.next {
  z-index: 800;
}
.carousel__photo.prev {
  transform: translateX(-100%); /* Move 'prev' item to the left */
}
.carousel__photo.next {
  transform: translateX(100%); /* Move 'next' item to the right */
}

.carousel__button--prev,
.carousel__button--next {
  position: absolute;
  top:50%;
  width: 3rem;
  height: 3rem;
  background-color: #FFF;
  transform: translateY(-50%);
  border-radius: 50%;
  cursor: pointer; 
  z-index: 1001; /* Sit on top of everything */
  border: 1px solid black;
}
.carousel__button--prev {
  left:0;
}
.carousel__button--next {
  right:0;
}
.carousel__button--prev::after,
.carousel__button--next::after {
  content: " ";
  position: absolute;
  width: 10px;
  height: 10px;
  top: 50%;
  left: 54%;
  border-right: 2px solid black;
  border-bottom: 2px solid black;
  transform: translate(-50%, -50%) rotate(135deg);
}
.carousel__button--next::after {
  left: 47%;
  transform: translate(-50%, -50%) rotate(-45deg);
}
`

export default class Carousel extends HTMLElement {

    constructor() {
        super()
        this.attachShadow({ mode: 'open' })
        this.initStyles();
    }


    slideClassName = 'carousel__photo'
    books = []
    slidesRef: HTMLElement = null
    carouselRef: HTMLElement = null
    bookElements = []
    sliding = true
    currentSlide = 0
    windowIsActive = true
    loading = false
    query = ''

    initStyles() {
        const stylesEl = document.createElement('style');
        stylesEl.innerHTML = styles;
        this.shadowRoot.appendChild(stylesEl);
    }

    connectedCallback(): void {

        // setup event listeners to watch the global state
        document.addEventListener('onSearchEnd', ((e: CustomEvent) => {
            this.books = e.detail.books.docs.filter((book: Object) => book.hasOwnProperty('cover_i'));

            this.sliding = false;
            this.currentSlide = 0;
            this.loading = false;
            this.render();


        }) as EventListener);

        document.addEventListener('onSearchBegin', ((e: CustomEvent) => {
            this.query = e.detail.value;
            this.loading = true
            this.render();
        }) as EventListener);

        this.render()
    }

    render() {
        // @TODO: A little messy, refactor to make it simple
        const element = document.createElement('div');
        element.classList.add('carousel-wrapper');

        if (this.loading) {
            element.innerHTML = `finding book covers....`;
            this.shadowRoot.replaceChildren(element)
        } else {
            if (this.books.length > 0) {
                element.innerHTML = `
                    <div class="carousel">
                        <div class="slides">
                        </div>
                        
                        <div class="carousel__button--next"></div>
                        <div class="carousel__button--prev"></div>
                    </div>
      `
                this.slidesRef = element.querySelector('.slides');
                this.carouselRef = element.querySelector('.carousel')
                this.shadowRoot.replaceChildren(element)
                this.initStyles();

                this.generateSlides();
                this.setInitialClasses();
                this.setInitialEventListeners();
                this.autoSlide();
            } else {
                element.innerHTML = ``;
                this.shadowRoot.replaceChildren(element)
            }
        }
    }

    generateSlides() {
        const bookElements = this.books.map((book, index) => {
            const el = new Image();
            el.dataset.src = `http://covers.openlibrary.org/b/id/${book.cover_i}-M.jpg`
            el.src = ''
            el.classList.add(this.slideClassName)
            if (index === 0) {
                el.classList.add('initial')
            }
            return el;
        });

        //preload first image
        this.preloadImage(bookElements[0]);

        //render all images
        (this.slidesRef as ParentNode).replaceChildren(...bookElements)
    }

    setInitialClasses() {
        const covers = this.items
        covers[this.totalItems - 1].classList.add("prev");
        covers[0].classList.add("active");
        covers[1].classList.add("next");
    }

    setInitialEventListeners() {
        const next = this.carouselRef.querySelector('.carousel__button--next');
        const prev = this.carouselRef.querySelector('.carousel__button--prev');
        next.addEventListener('click', this.slideNext);
        prev.addEventListener('click', this.slidePrev);

        // setup event listeners to detect when window is active
        window.onfocus = () => { this.windowIsActive = true }
        window.onblur = () => { this.windowIsActive = false }
    }

    get items() {
        return this.slidesRef.querySelectorAll(`.${this.slideClassName}`)
    }

    get totalItems() {
        return this.items.length
    }

    slideNext = () => {
        if (!this.sliding) {
            if (this.currentSlide === (this.totalItems - 1)) {
                this.currentSlide = 0;
            } else {
                this.currentSlide = this.currentSlide + 1;
            }

            this.slideCarouselTo(this.currentSlide);
        }
    }

    slidePrev = () => {
        if (!this.sliding) {
            if (this.currentSlide === 0) {
                this.currentSlide = (this.totalItems - 1);
            } else {
                this.currentSlide = this.currentSlide - 1;
            }

            this.slideCarouselTo(this.currentSlide);
        }
    }

    slideCarouselTo(slide) {
        if (!this.sliding) {
            this.disableInteraction();

            var newPrevious = slide - 1,
                newNext = slide + 1,
                oldPrevious = slide - 2,
                oldNext = slide + 2;

            if ((this.totalItems - 1) > 3) {
                if (newPrevious <= 0) {
                    oldPrevious = (this.totalItems - 1);
                } else if (newNext >= (this.totalItems - 1)) {
                    oldNext = 0;
                }
            }

            if (slide === 0) {
                newPrevious = (this.totalItems - 1);
                oldPrevious = (this.totalItems - 2);
                oldNext = (slide + 1)
            } else if (slide === (this.totalItems - 1)) {
                newPrevious = (slide - 1);
                newNext = 0;
                oldNext = 1;
            }

            this.items[oldPrevious].className = this.slideClassName;
            this.items[oldNext].className = this.slideClassName;

            this.preloadImage(this.items[slide]);
            this.preloadImage(this.items[newNext]);

            this.items[newPrevious].className = this.slideClassName + " prev";
            this.items[slide].className = this.slideClassName + " active";
            this.items[newNext].className = this.slideClassName + " next";
        }
    }

    autoSlide() {
        // auto slide to next every two seconds
        setInterval(() => {
            if (this.windowIsActive)
                this.slideNext();
        }, 2 * 1000)
    }

    disableInteraction() {
        // Set 'sliding' to true for the same duration as our transition.
        // (0.5s = 500ms)

        this.sliding = true;
        // setTimeout runs its function once after the given time
        setTimeout(() => {
            this.sliding = false
        }, 500);
    }


    preloadImage(img) {
        const src = img.getAttribute('data-src');
        if (!src) { return; }
        img.src = src;
      }

}

