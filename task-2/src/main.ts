import Title from './title'
import Nav from './nav'
import Carousel from './carousel'

customElements.define('search-title', Title)
customElements.define('my-nav', Nav)
customElements.define('my-carousel', Carousel)