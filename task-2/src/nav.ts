
const styles = `

@media (min-width: 768px) { 
    .nav {
        flex-direction: row;
    }
}


* {
    font-family: 'Share Tech Mono', monospace;
}

.nav {
    display: flex;
    justify-content: center;
    flex-direction: column;
    padding: 20px 0;
    align-items: center;
}

.logo {
    font-size: 24px;
    padding: 8px;
    color: var(--blue);
    
}

.search {
    display: flex;

}

.search-form {
    display: flex;
    border: 2px solid var(--blue);
    border-radius: 32px;
    padding: 8px 12px;
    width: 60vw;
    margin-left: 12px;
}

.search-form input {
    width: calc(100% - 72px);
    border: 0;
    color: var(--blue);
}

.search-form input:focus {
    outline: unset;
}

button {
    width: fit-content;
    align-self: center;
    background-color: var(--blue);
    margin: 0  8px;
    padding: 12px 32px;
    color: white;
    border-radius: 8px;
    border: 0;
    cursor: pointer;
}

button:focus {
    outline: unset
}

.main {
    width: 100vw;
}

.carousel {

}

.slides-wrapper {
    padding:0 ;
    margin:0;
}

.slide > * {
    width: 100%;
}

`

class Nav extends HTMLElement {
    constructor() {
        super();

        const shadow = this.attachShadow({ mode: 'open' });
        const stylesEl = document.createElement('style');
        stylesEl.innerHTML = styles

        const navContainer = document.createElement('nav');

        navContainer.classList.add('nav');

        navContainer.innerHTML = `
        <div class="logo"> Dott Search </div>
            <div class="search">
                <div class="search-form">
                    <object id="mic">
                        <svg width="32" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M19 23.75C21.6283 23.75 23.7342 21.6283 23.7342 19L23.75 9.5C23.75 6.87167 21.6283 4.75 19 4.75C16.3717 4.75 14.25 6.87167 14.25 9.5V19C14.25 21.6283 16.3717 23.75 19 23.75ZM17.1 9.34167C17.1 8.29667 17.955 7.44167 19 7.44167C20.045 7.44167 20.9 8.29667 20.9 9.34167L20.8842 19.1583C20.8842 20.2033 20.045 21.0583 19 21.0583C17.955 21.0583 17.1 20.2033 17.1 19.1583V9.34167ZM27.3917 19C27.3917 23.75 23.37 27.075 19 27.075C14.63 27.075 10.6083 23.75 10.6083 19H7.91667C7.91667 24.3992 12.2233 28.8642 17.4167 29.64V34.8333H20.5833V29.64C25.7767 28.88 30.0833 24.415 30.0833 19H27.3917Z"
                                fill="#28256B" />
                        </svg>
                    </object>
                    <input type="text" placeholder="search for...">
                    <object>
                        <svg width="32" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M23.25 21H22.065L21.645 20.595C23.1658 18.831 24.0017 16.579 24 14.25C24 12.3216 23.4282 10.4366 22.3568 8.83319C21.2855 7.22981 19.7627 5.98013 17.9812 5.24218C16.1996 4.50422 14.2392 4.31114 12.3479 4.68734C10.4566 5.06355 8.71928 5.99215 7.35571 7.35571C5.99215 8.71927 5.06355 10.4566 4.68735 12.3479C4.31114 14.2392 4.50422 16.1996 5.24218 17.9812C5.98013 19.7627 7.22982 21.2855 8.83319 22.3568C10.4366 23.4282 12.3216 24 14.25 24C16.665 24 18.885 23.115 20.595 21.645L21 22.065V23.25L28.5 30.735L30.735 28.5L23.25 21V21ZM14.25 21C10.515 21 7.5 17.985 7.5 14.25C7.5 10.515 10.515 7.5 14.25 7.5C17.985 7.5 21 10.515 21 14.25C21 17.985 17.985 21 14.25 21Z"
                                fill="#28256B" />
                        </svg>
                    </object>
                </div>
                <button>Search</button>
            </div>
        `
        this.inputRef = navContainer.querySelector('input')
        this.inputRef.onchange = this.onChange
        navContainer.querySelector('#mic').onclick = this.captureVoice
        shadow.appendChild(stylesEl);
        shadow.appendChild(navContainer);
    }

    inputRef: HTMLInputElement =  null

    render() {

    }

    captureVoice = (e) => {
        if (!('webkitSpeechRecognition' in window)) {
            window.upgrade();
        } else {
            var recognition = new webkitSpeechRecognition();
            // set params
            recognition.continuous = false;
            recognition.interimResults = false;
            recognition.start();

            recognition.onresult = (event) => {

                // delve into words detected results & get the latest
                // total results detected
                var resultsLength = event.results.length - 1;
                // get length of latest results
                var ArrayLength = event.results[resultsLength].length - 1;
                // get last word detected
                var saidWord: string = event.results[resultsLength][ArrayLength].transcript;
                if(saidWord.startsWith('search for')){
                    const query = saidWord.split('search for ')[1];
                    this.inputRef.value = query
                    const event = new Event('change');
                    this.inputRef.dispatchEvent(event);
                }
            }

            // speech error handling
            recognition.onerror = function (event) {
                console.error(event);
                alert("Try saying, 'Search for dog' ");
            }
        }
    }

    onChange = (e) => {
        const { value } = e.target;
        this.search(value)
    }

    async search(value: string) {
        document.dispatchEvent(new CustomEvent("onSearchBegin", {
            bubbles: true,
            detail: {
                value
            }
        }));
        await fetch(`https://openlibrary.org/search.json?title=${value}`, {}).then((res: Response) => {
            res.json().then(data => {
                document.dispatchEvent(new CustomEvent("onSearchEnd", {
                    bubbles: true,
                    detail: {
                        books: data
                    }
                }))
            })
        })
    }

}


export default Nav;
