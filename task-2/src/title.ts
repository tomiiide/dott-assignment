
const styles = `
  h4{
    margin: 4px 0;
  }

  small {
    font-size: 10px;
  }
`

export default class Title extends HTMLElement {

  constructor() {
    super()
    this.attachShadow({ mode: 'open' })
  }

  timestamp: number = Date.now();
  loading: boolean = true;
  query: string = '';
  books: Object[] = []

  get lastUpdated() {
    return new Date(this.timestamp).toLocaleTimeString(window.navigator.language)
  }

  connectedCallback(): void {
    //setup event listeners
    document.addEventListener('onSearchEnd', ((e: CustomEvent) => {
      this.timestamp = Date.now();
      this.loading = false
      this.books = e.detail.books.docs.filter((book: Object) => book.hasOwnProperty('cover_i'));
      this.render();

    }) as EventListener);

    document.addEventListener('onSearchBegin', ((e: CustomEvent) => {
      this.query = e.detail.value;
      this.loading = true
      this.render();
    }) as EventListener);

    this.render()
  }

  initStyles() {
    const stylesEl = document.createElement('style');
    stylesEl.innerHTML = styles;
    this.shadowRoot.appendChild(stylesEl);
  }

  render() {
    if (!this.loading) {
      if (this.books.length > 0 && this.query.length > 0) {
        this.shadowRoot.innerHTML = `
      <h4>Search Results for '${this.query}'</h4>
      <small>Last updated: ${this.lastUpdated} </small>
    `} else {
        this.shadowRoot.innerHTML = `
      <h4>No covers found for '${this.query}' </h4>
      <small>Last updated: ${this.lastUpdated} </small>
      `;
      }
    } else {
      this.shadowRoot.innerHTML = `
      `
    }
    this.initStyles();
  }

}
